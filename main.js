let tab = function () {
let tabsTitle = document.querySelectorAll('.tabs-title');
let tabContent = document.querySelectorAll('.tabs-panel');
let tabName;

tabsTitle.forEach(item => {
item.addEventListener('click', selectTabsTitle)
});
function selectTabsTitle() {
tabsTitle.forEach(item => {
item.classList.remove('active');
});
this.classList.add('active');
tabName = this.getAttribute('data-tab-name');
selectTabContent(tabName);
}
function selectTabContent(tabName) {
tabContent.forEach(item => {
item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
})
}
};
tab();